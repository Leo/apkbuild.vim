" Author: Leo <thinkabit.ukim@gmail.com>
" Description: Use a modified shellcheck invocation and grep to lint APKBUILDs

runtime! ale_linters/sh/shellcheck.vim

call ale#linter#Define('apkbuild', {
\	'name': 'shellcheck',
\	'executable': apkbuild#shellcheck(),
\	'command': '%e %t',
\	'callback':	'ale#handlers#shellcheck#Handle',
\})
