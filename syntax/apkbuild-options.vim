" Vim syntax file
" Language:		apkbuild
" Maintainer:	Leo <thinkabit.ukim@gmail.com>
" Last Change:	2021-07-21

" the `options=` keyword
syn keyword AbuildOptionsKeyword options contained nextgroup=shOperator
" Make it like other keywords
hi def link AbuildOptionsKeyword AbuildKeywords

" All the valid options, this is extraced from `abuild` itself on a
" best-effort basis
syn match AbuildValidOptions /\v(!archcheck|!check|checkroot|net|!strip|suid|!tracedeps|chmod-clean|!dbg|toolchain|!fhs|libtool|charset.alias|textrels|!spdx|ldpath-recursive|sover-namecheck|lib64)/ contained
syn match AbuildOptionsRef /\$options/ contained containedin=AbuildInvalidOptions

" Match everything until the end
syn region AbuildOptionsGroup start=/options="/ end=/"/ contains=AbuildOptionsKeyword keepend

" To simulate what sh.vim does with `shDoubleQuote->shQuote` 
syn region AbuildOptionsValue start=/"/ end=/"/ contained containedin=AbuildOptionsGroup
syn match AbuildOptionsQuote /"/ contained containedin=AbuildOptionsValue

" Mark it as
hi def link AbuildOptionsValue String
hi def link AbuildOptionsQuote String

" Treat it like a shell does
hi def link AbuildOptionsRef shDerefSimple

hi def link AbuildValidOptions String
hi def link AbuildInvalidOptions Error

" Use :blank: to match whitespace and tabs
syn match AbuildInvalidOptions /[^"[:blank:]]/ contained contains=AbuildValidOptions containedin=AbuildOptionsValue
