" Vim syntax file
" Language:		apkbuild
" Maintainer:	Leo <thinkabit.ukim@gmail.com>
" Last Change:	2021-07-21

" Start the block with '^# secfixes:' and end at the first line that doesn't
" start with #
" This is done by:
" 1. Enabling 'very magic' (\v)
" 2. Matching a line that starts with '#'
" 3. Matching non-greedy an empty line (\n\n) or a line that does not start
" with '#'
syn region AbuildSecfixesGroup start=/^# secfixes:/ end=/\v^#*(\n\n|^[^#]){-}/ containedin=AbuildComment

" The title line of a secfixes block
syn match AbuildSecfixesTitle /secfixes:/ contained contains=AbuildSecfixesColon containedin=AbuildSecfixesGroup

" Right now we just match everything after 3 whitespace, in the future we
" want to use a globally defined AbuildPkgver which can robustly handle
" finding the correct pkgver, maybe an AbuildPkgver which has multiple
" contains= like AbuildPkgverPreSuffixes and AbuildPkgverPostfixes for dealing
" with special tokens like _git _hg
syn match AbuildSecfixesVersionLine /^#\s\{3}[0-9].*/ contained contains=AbuildSecfixesColon containedin=AbuildSecfixesGroup
" A regex match for valid versions to highlight in a secfixes group
syn match AbuildSecfixesPkgverPkgrel /\v(\v([A-Za-z]{,1}|[0-9\.])*-r[[:digit:]]|0)/ contained containedin=AbuildSecfixesVersionLine

" The whole line where an identifier is, it must start with 5 whitespaces
syn match AbuildSecfixesIdentifierLine /^#\s\{5}- .*$/ contained containedin=AbuildSecfixesGroup
" The dash in the `- $IDENTIFIER` part
syn match AbuildSecfixesIdentifierDash /-/ contained containedin=AbuildSecfixesIdentifierLine
" Expected format: CVE-YYYY-XXXX+
syn match AbuildSecfixesCVE /CVE-\d\{4}-\d\{4,}/ contained containedin=AbuildSecfixesIdentifierLine
" Expected format: XSA-X+
syn match AbuildSecfixesXSA /XSA-\d\{1,}/ contained containedin=AbuildSecfixesIdentifierLine
" Expected format: GNUTLS-SA-YYYY-MM-DD
syn match AbuildSecfixesGNUTLS /GNUTLS-SA-\d\{4}-\d\{2}-\d\{2}/ contained containedin=AbuildSecfixesIdentifierLine

" Match any ':', we set it so we don't highlight it
" we use it in AbuildSecfixesTitle and AbuildSecfixesVersionLine
" to not highlight the colon in `secfixes:` and `$pkgver-r$pkgrel:`
syn match AbuildSecfixesColon /:/ contained

" Comments made after an identifier with '(Comment)' syntax
" Example: - CVE-2000-1000 (Not affected we hve --disable-foo)
syn match AbuildSecfixesComment /(.*)/ contained containedin=AbuildSecfixesIdentifierLine

" HashChar is a special match that is only the '#', it is used in secfixes
" to make the starting '#' of the comment section have a normal comment and
" allow us to only highlight important parts with 'SpecialComment'.
hi def link AbuildSecfixesGroup comment
hi def link AbuildSecfixesComment Comment
hi def link AbuildSecfixesColon Comment
hi def link AbuildSecfixesVersionLine Comment
hi def link AbuildSecfixesIdentifierLine Comment
