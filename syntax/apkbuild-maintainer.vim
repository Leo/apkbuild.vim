" Vim syntax file
" Language:		apkbuild
" Maintainer:	Leo <thinkabit.ukim@gmail.com>
" Last Change:	2021-07-21

" Capture the whole line
syn region AbuildMaintainerGroup start=/^# Maintainer:/ end=/$/ containedin=AbuildComment oneline keepend

" The `Maintainer` keyword
syn match AbuildMaintainerKeyword /Maintainer/ contained containedin=AbuildMaintainerGroup

syn match AbuildMaintainerValue /:.*/ contained containedin=AbuildMaintainerGroup
syn match AbuildMaintainerColon /:/ contained containedin=AbuildMaintainerValue

" Make everything and the ':' a comment value, the rest which is the
" 'Maintainer' keyword and its value will be highlighted
hi def link AbuildMaintainerGroup Comment
hi def link AbuildMaintainerColon Comment
