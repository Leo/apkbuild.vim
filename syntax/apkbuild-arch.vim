" Vim syntax file
" Language:		apkbuild
" Maintainer:	Leo <thinkabit.ukim@gmail.com>
" Last Change:	2021-07-21

" All highlighting related to the `arch` keyword

" Capture the whole string from that makes up the `arch=` variable
syn region AbuildArchGroup start=/^arch="/ end=/"/ contains=AbuildArchKeyword keepend

" the keyword arch, make it part of the AbuildArchGroup
" mark the '=' as part of `shOperator` instead of `shVarAssign`
" using the latter will cause it to automatically use the
" `shDoubleQuote->shQuote` logic which prevents us from using our own logic to
" get some nice error highlighting when the user inputs invalid arches
syn keyword AbuildArchKeyword arch contained nextgroup=shOperator

" This is a big fancy regex for all the arches we recognize, look at the
" arch_to_hostspec() function in /usr/share/abuild/functions.sh it should list
" all the arches abuild deals with
"
" List of arches we check:
" - aarch64
" - armel
" - armhf
" - armv7
" - mips
" - mips64
" - mipsel
" - mips64el
" - ppc
" - ppc64
" - ppc64le
" - riscv32
" - riscv64
" - s390x
" - x86
" - x86_64
" - $arch (this is one we use to detect usage of the $arch variable)
" Please keep it in the same order arch_to_hostpec()
syn match AbuildArchARM /\v(!)?(aarch64|arm(el|hf|v7))/ contained containedin=AbuildInvalidArch
syn match AbuildArchMIPS /\v(!)?mips(el)?(64(el)?)?/ contained containedin=AbuildInvalidArch
syn match AbuildArchPowerPC /\v(!)?ppc(64(le)?)?/ contained containedin=AbuildInvalidArch
syn match AbuildArchRiscV /\v(!)?riscv(32|64)/ contained containedin=AbuildInvalidArch
syn match AbuildArchIBM /\v(!)?s390x/ contained containedin=AbuildInvalidArch
syn match AbuildArchIntel /\v(!)?x86(_64)?/ contained containedin=AbuildInvalidArch
syn match AbuildArchRef /\$arch/ contained containedin=AbuildInvalidArch

" These are keywords we have, `all` refers to all arches and `noarch` refers
" to something that has no machine code (like python modules)
syn keyword AbuildArchliases all noarch contained containedin=AbuildArchValue

" Match anything between the quotes and mark it as an error, then put
" AbuildValidArch inside it
syn match AbuildInvalidArch /[^"[:blank:]]/ contained containedin=AbuildArchValue

" To simulate what sh.vim does with `shDoubleQuote->shQuote` 
syn region AbuildArchValue start=/"/ end=/"/ contained containedin=AbuildArchGroup
syn match AbuildArchQuote /"/ contained containedin=AbuildArchValue

" Mark it as
hi def link AbuildArchValue String
hi def link AbuildArchQuote String

" Connect AbuildArchKeyword to the more general AbuildKeywords
hi def link AbuildArchKeyword AbuildKeywords

" Link all Arch groups into a single arch
hi def link AbuildArchARM AbuildValidArches
hi def link AbuildArchMIPS AbuildValidArches
hi def link AbuildArchPowerPC AbuildValidArches
hi def link AbuildArchRiscV AbuildValidArches
hi def link AbuildArchIBM AbuildValidArches
hi def link AbuildArchIntel AbuildValidArches
hi def link AbuildValidArches String

" Treat it like shell does
hi def link AbuildArchRef shDerefSimple

hi def link AbuildArchliases String
hi def link AbuildInvalidArch Error
