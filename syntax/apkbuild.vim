" Vim syntax file
" Language:		apkbuild
" Maintainer:	Leo <thinkabit.ukim@gmail.com>
" Last Change:	2021-07-21
if exists('b:current_syntax')
  finish
endif

let b:main_syntax = 'sh'
" This should stay as is_posix as long as there is no is_ash
" in the meantime we should vendor the sections we want that
" provide the highlighting in APKBUILD that we use.
let b:is_posix = 1

runtime! syntax/sh.vim
" Holds parts of sh.vim that are guarded under is_bash but are used by Alpine
" Linux's ash shell
runtime! syntax/apkbuild-vendored.vim

" Definitions for the `pkgname=` variable
runtime! syntax/apkbuild-pkgname.vim

" Definitions for the `secfixes:` block that is embedded in comments
runtime! syntax/apkbuild-secfixes.vim

" Definitions for the `arch=` variable
runtime! syntax/apkbuild-arch.vim

" Definitions for the `options=` variable
runtime! syntax/apkbuild-options.vim

" Definitions for the `Maintainer:` block that is embedded in comments
runtime! syntax/apkbuild-maintainer.vim

" Definitions for the `pkgrel=`
runtime! syntax/apkbuild-pkgrel.vim

" case on
syn case match

" Variables used by 'abuild'
syn keyword AbuildKeywords startdir srcdir pkgdir subpkgdir builddir
    \ depends depends_dev depends_doc depends_openrc
    \ dedpends_libs depends_static checkdepends giturl
    \ install install_If license makedepends makedepends_build
    \ makedepends_host sha256sums sha512sums pkgdesc
    \ pkggroups pkgusers provides
    \ provider_priority replaces replaces_priority source
    \ subpackages triggers ldpath linguas sonameprefix somask
    \ url langdir patch_args pcprefix HOSTCC
    \ nextgroup=shVarAssign

" Banned variables
syn keyword AbuildDeprecatedKeywords md5sums containedin=shVariable

" 'abuild' functions
syn keyword AbuildFunctions fetch unpack dev doc openrc static snapshot prepare
syn keyword AbuildFunctions prepare build check package

" Match everything until the first '_' which is the suffix
syn match AbuildPkgverNoSuffix /.\{-}\ze\(\s\|_\)/ contained containedin=AbuildPkgverValue

" This match what is mostly expected of pkgver, a collection
" of numbers from 0 to 9 and dots, semver works this way with
" X.Y.Z
syn keyword AbuildPkgverKeyword pkgver contained nextgroup=shOperator
hi def link AbuildPkgverKeyword AbuildKeywords
syn match AbuildPkgverValid /\v([A-Za-z]{,1}|[0-9\.])/ contained containedin=AbuildPkgverNoSuffix
syn match AbuildPkgverInvalid /\v([^A-Za-z0-9\.]|[A-Za-z]{2,})/ contained containedin=AbuildPkgverNoSuffix

" There are all the possible sufixes that can be used according to version.c
" in apk-tools, optionally match against a single underline (_) so that the
" later AbuildPkgverInvalidSuffixes doesn't match it when dealing with
" numbers, hyphens or more underlines following an underline.
syn match AbuildPkgverSuffixKeywords /\v(_)?(cvs|svn|git|hg|p(re)?|alpha|beta|rc)/ contained
syn match AbuildPkgverSuffixes /_[^ ]\{-}\ze/ contained contains=AbuildPkgverSuffixKeywords,AbuildPkgverInvalidSuffixes containedin=AbuildPkgverValue

syn match AbuildPkgverValue /[^ ]/ contained contains=shDerefSimple containedin=AbuildPkgverLine

syn match AbuildPkgverAfterValue / \zs.*\ze\(\s\|$\)/ contained containedin=AbuildPkgverLine contains=AbuildComment

" Match anything after an underline (_) that is not a latter, this will
" match all words until we reach AbuildPkgverSuffixKeywords, also in all
" other occasions match anything that is not a digit, only digits and keywords
" are allowed in suffixes
syn match AbuildPkgverInvalidSuffixes /\v(_[^A-Za-z]*|[^[:digit:]])/ contained contains=AbuildPkgverSuffixKeywords

syn match AbuildPkgverLine /.*/ contained containedin=AbuildPkgver contains=AbuildComment

" Match everything until a hash, line-end, or whitespace
syn region AbuildPkgver start=/pkgver=/ end=/$/ oneline contains=AbuildPkgverKeyword

" Inside AbuildPkgnameAfterValue match everything that isn't a whitespace or a
" hash
syn match AbuildPkgverStrayChar /[^ #]/ contained containedin=AbuildPkgverAfterValue

" Comments
syn keyword	AbuildTodo contained COMBAK FIXME TODO XXX
syn match	AbuildComment "^\s*\zs#.*$" contains=@AbuildCommentGroup
syn match	AbuildComment "\s\zs#.*$" contains=@AbuildCommentGroup

hi def link AbuildComment Comment
hi def link AbuildTodo Todo

hi def link AbuildPkgverInvalid Error
hi def link AbuildPkgverInvalidSuffixes Error
hi def link AbuildPkgverStrayChar Error

hi def link AbuildKeywords				Keyword
hi def link AbuildDeprecatedKeywords	Error
hi def link AbuildFunctions				Function

let b:current_syntax = 'APKBUILD'
