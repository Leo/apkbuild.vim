" Vim syntax file
" Language:		apkbuild
" Maintainer:	Leo <thinkabit.ukim@gmail.com>
" Last Change:	2021-07-21

" Mark it as a keyword, make it sure that the next operator is '='
syn keyword AbuildPkgnameKeyword pkgname contained nextgroup=shOperator
" Link its highlighting to all other keywords from abuild
hi def link AbuildPkgnameKeyword AbuildKeywords

" Match the whole line
syn region AbuildPkgname start=/pkgname=/ end=/$/ oneline contains=AbuildPkgnameKeyword

" Policy dictates that all packages need to have lowercase characters, 'R' is
" a notable exception, apk seems to almost anything thrown at it but the
" reserved characters like '@' (repository pinning).
" '=' caused 'bad specifier' errors
" '%' causes premature end of the name 'foo%bar' will produce successfully a
"	'foo' package, but this seems related to abuild and shell
" '&' is shell-related and anything after it is executed as a command
" ' ' whitespace is bad, use - or _
syn match AbuildPkgnameBadChar /[A-Z@%&="']/ contained containedin=AbuildPkgnameValue

" Match the whole line, everything will submatch from it
syn match AbuildPkgnameLine /.*/ contained containedin=AbuildPkgname contains=AbuildComment

" Match everything that isn't a whitespace and make the whitespace
" non-inclusive so it ends on AbuildPkgnameLine
syn match AbuildPkgnameValue /[^ ]/ contained contains=shDerefSimple containedin=AbuildPkgnameLine

" Match everything after the first space and match it until the last space but
" don't include it
syn match AbuildPkgnameAfterValue / \zs.*\ze\(\s\|$\)/ contained containedin=AbuildPkgnameLine contains=AbuildComment

" Inside AbuildPkgnameAfterValue match everything that isn't a whitespace or a
" hash
syn match AbuildPkgnameStrayChar /[^ #]/ contained containedin=AbuildPkgnameAfterValue


hi def link AbuildPkgnameBadChar Error
hi def link AbuildPkgnameStrayChar Error
