" Vim syntax file
" Language:		apkbuild
" Maintainer:	Leo <thinkabit.ukim@gmail.com>
" Last Change:	2021-07-21

" pkgrel
syn keyword AbuildPkgrelKeyword pkgrel contained nextgroup=shOperator
hi def link AbuildPkgrelKeyword AbuildKeywords

" match the whole line
syn region AbuildPkgrel start=/pkgrel=/ end=/$/ oneline contains=AbuildPkgrelKeyword

" Match the whole line, everything will submatch from it
syn match AbuildPkgrelLine /.*/ contained containedin=AbuildPkgrel contains=AbuildComment

" Match everything that isn't a whitespace and make the whitespace
" non-inclusive so it ends on AbuildPkgrelLine
syn match AbuildPkgrelValue /[^ ]/ contained contains=shDerefSimple containedin=AbuildPkgrelLine

" Match everything after the first space and match it until the last space but
" don't include it
syn match AbuildPkgrelAfterValue / \zs.*\ze\(\s\|$\)/ contained containedin=AbuildPkgrelLine contains=AbuildComment

" Match anything that is not a digit or =, also check if we have a 0 that is
" followed by any other number, if 0 is the leading digit (right after =) then
" consider any digit after it as Invalid because you can't have 03 as pkgrel
" but you can have 203
" syn match AbuildInvalidPkgrel /[^[:digit:]]/ contained containedin=AbuildPkgrelValue
syn match AbuildInvalidPkgrelNotDigit /[^[:digit:]]/ contained containedin=AbuildPkgrelValue
syn match AbuildInvalidPkgrelLeadingZero "\(=0\)\@<=[^ ]*" contained containedin=AbuildPkgrelValue

hi def link AbuildInvalidPkgrelNotDigit AbuildInvalidPkgrel
hi def link AbuildInvalidPkgrelLeadingZero AbuildInvalidPkgrel
hi def link AbuildInvalidPkgrel Error
hi def link AbuildPkgrelAfterValue Error
