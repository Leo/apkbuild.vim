# APKBUILD.vim

# Description

APKBUILD.vim is a Vim (and Neovim!) plugin which helps editing and working with
APKBUILD files.

An APKBUILD is a build recipe used by Alpine Linux's abuild for creating .apk
packages.

# Installation

## [vim-plug][1]

```vim
Plug 'https://gitlab.alpinelinux.org/Leo/apkbuild.vim.git'
```

# Components

## ftdetect

Checks if file is named APKBUILD and sets filetype=apkbuild.

## indent

Sources indent/sh.vim but also sets `noexpandtab` and `textwidth=80`.

## syntax

Sets main\_syntax to 'sh' and sources syntax/sh.vim, so it should have the same
effect as setting filetype=sh.

## scripts

Scripts that are used by other functions here of APKBUILD.vim are stored here.

### shellcheck\_APKBUILD.sh

Wrapper around `shellcheck` that filters out errors and warnings that are not valid
in APKBUILDs, example: variables like `pkgdir` may seem not set but are actually set
by `abuild`.

It ignores the follow shellcheck directives:

  - SC2164
  - SC2016
  - SC2086
  - SC2169
  - SC2155
  - SC2100
  - SC2209
  - SC2130
  - SC2131
  - SC1090

It also keeps a list of all variables which are used by `abuild` in the variables called
`variables` and filters the following messages matching against them:

- `($variables) appears unused`
- `($variables) is referenced but not assigned`
- `($variables) may not be assigned`

## ale\_linter/APKBUILD

linters for [ALE][2] are stored here.

### shellcheck

The simplest of the linters, it calls the `shellcheck_APKBUILD.sh` in scripts/
and uses shellcheck linting to handle the output.

### apkbuild\_lint

It calls `aports-lint` from [atools][3] and parses it's output.

The expected format in ALE is: `TAG: MSG`, where `TAG` is a number given to a specific
policy violation and `MSG` is the message given by `aports-lint` describing the error.

Example:
  - TAG: `AL5`, empty variable that can be removed
  - MSG: `variable set to empty string: $variable=""`

The output of the situation above should be:

```
AL5: variable set to empty string: depends=""
```

### secfixes\_check

It calls `secfixes-check` from [atools][3] and parses it's output.

The expected format in ALE is: `TAG: MSG`, where `TAG` is a number given to a specific
policy violation and `MSG` is the message given by `secfixes-check` describing the error.

Example:
  - TAG: `AL59`, duplicate value defined in secfixes section of the APKBUILD
  - MSG: `duplicate value 'CVE-XXXX-YYYY'`

The output of the situation above should be:

```
AL59: duplicate value 'CVE-XXXX-YYYY'
```

# Credit

Ideas, structure and some code were copied/adapted from [vim-pkgbuild][4] by
Martition Pilia and [PKGBUILD.vim][5] by Fernando Gilberto Pereira da Silva.

# License

The code is licensed under MIT, see LICENSE.

[1]:	https://github.com/junegunn/vim-plug
[2]:	https://github.com/dense-analysis/ale
[3]:	https://gitlab.alpinelinux.org/Leo/atools
[4]:	https://github.com/m-pilia/vim-pkgbuild
[5]:	https://github.com/Firef0x/PKGBUILD.vim/
