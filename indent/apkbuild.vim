" Vim indent file
" Language:		apkbuild
" Maintainer:	Leo <thinkabit.ukim@gmail.com>
" Last Change:	2020-11-01
if exists('b:did_indent')
	finish
endif

runtime! indent/sh.vim
let b:did_indent = 1
