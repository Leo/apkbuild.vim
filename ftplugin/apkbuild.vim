" Vim filetype detect file
" Language:		apkbuild
" Maintainer:	Leo <thinkabit.ukim@gmail.com>
" Last Change:	2020-11-03
" URL:			https://gitlab.alpinelinux.org/APKBUILD.vim
if exists('b:did_ftplugin')
  finish
endif

runtime! ftplugin/sh.vim
let b:did_ftplugin = 1

setlocal noexpandtab
setlocal softtabstop=0
setlocal shiftwidth=4
setlocal tabstop=4
setlocal textwidth=80

" Register apkbuild_fixer in registry
execute ale#fix#registry#Add(
\   'apkbuild_fixer', 
\   'ale#fixers#apkbuild_fixer#Fix',
\   ['apkbuild'],
\   'Fix policy violations found by apkbuild-lint in APKBUILDs'
\)

" Add apkbuild_fixer as a fixer
let b:ale_fixers = ['apkbuild_fixer']
