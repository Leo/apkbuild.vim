" Vim filetype detect file
" Language:		apkbuild
" Maintainer:	Leo <thinkabit.ukim@gmail.com>
" Last Change:	2020-11-01
" URL:			https://gitlab.alpinelinux.org/APKBUILD.vim
autocmd BufNewFile,BufRead APKBUILD set filetype=apkbuild
