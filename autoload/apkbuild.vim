" Vim autoload file
" Language:		apkbuild
" Maintainer:	Leo <thinkabit.ukim@gmail.com>
" Last Change:	2020-11-01
let s:linter = expand('<sfile>:p:h:h') . '/scripts/shellcheck_apkbuild.sh'

function! apkbuild#shellcheck() abort
  return s:linter
endfunction
