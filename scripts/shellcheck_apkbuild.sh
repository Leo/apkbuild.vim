#!/bin/sh
variables=$(echo -n "#.*
_.*
startdir
srcdir
pkgdir
subpkgdir
builddir
arch
depends
depends_dev
depends_doc
depends_openrc
depends_libs
depends_static
checkdepends
giturl
install
.*.pre-install
.*.post-install
.*.pre-upgrade
.*.post-upgrade
.*.pre-deinstall
.*.post-deinstall
install_if
license
makedepends
makedepends_build
makedepends_host
md5sums
sha256sums
sha512sums
options
pkgdesc
pkggroups
pkgname
pkgrel
pkgusers
pkgver
provides
provider_priority
replaces
replaces_priority
source
subpackages
triggers
ldpath
linguas
sonameprefix
somask
url
langdir
patch_args
pcprefix
HOSTCC
CFLAGS
CXXFLAGS
CPPFLAGS
LDFLAGS
JOBS
MAKEFLAGS" | tr '\n' '|')

shellcheck -s ash  \
		   -f gcc \
		   -e SC2164 \
		   -e SC2016 \
   		   -e SC2086 \
   		   -e SC2169 \
   		   -e SC2155 \
   		   -e SC2100 \
   		   -e SC2209 \
   		   -e SC2030 \
   		   -e SC2031 \
   		   -e SC1090 \
		   "$@" \
  | grep -E -v \
		 -e "($variables) appears unused" \
		 -e "($variables) is referenced but not assigned" \
		 -e "($variables) may not be assigned"
